from django.db import models

class Autor(models.Model):
	CITIES = (
		('LA PAZ', 'LA PAZ'),
		('POTOSI', 'POTOSI'),
		)
	name = models.CharField(max_length=20, verbose_name = 'Nombres')
	city = models.CharField(max_length=30, choices = CITIES, verbose_name='Ciudad de Origen')
	fech_nac = models.DateField(verbose_name = 'Fecha de Nacimiento')
	edad = models.IntegerField(verbose_name='Edad', null = True, blank=True)
	coust = models.FloatField(verbose_name='Ganancias')
	state = models.BooleanField(default=False, verbose_name='Estado del Autor')
	email = models.EmailField(verbose_name='Correo Electronico')

	def __str__(self):
		#return self.name
		return '%s - %s' % (self.name, self.edad)

	class Meta:
		verbose_name = 'Autor'
		verbose_name_plural = 'Autores'
		ordering = ['city', 'name', 'edad']